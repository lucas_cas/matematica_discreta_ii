//General
import Vue from 'vue';
import Router from 'vue-router';
import Home from './home/Home';
import About from './about/About';

Vue.use(Router);
const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    }
  ]
});

export default router;