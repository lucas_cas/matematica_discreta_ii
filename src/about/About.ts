import { Vue, Component } from 'vue-property-decorator';
import Card from './../../cas_frontend/components/containers/card/Card';
import FlexLayout from './../../cas_frontend/components/containers/flex_layout/FlexLayout';
import SideNav from './../../cas_frontend/components/menus/sidenav/SideNav';
import App from './../app/App';

@Component({
    'components': { Card, FlexLayout, SideNav }
})

export default class About extends App {

    sideNavPages = App.getSideNavPages('about');

    mounted() {

    }

}