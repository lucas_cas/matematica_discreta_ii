import { Vue, Component } from 'vue-property-decorator';
import FlexLayout from './../../cas_frontend/components/containers/flex_layout/FlexLayout';
import BlockLayout from './../../cas_frontend/components/containers/block_layout/BlockLayout';
import Card from './../../cas_frontend/components/containers/card/Card';
import SideNav from './../../cas_frontend/components/menus/sidenav/SideNav';
import Button from './../../cas_frontend/components/fields/button/Button';
import InputWrapper from './../../cas_frontend/components/fields/input/InputWrapper';
import * as math from 'mathjs'
import App from './../app/App';

@Component({
    props: {},
    components: { FlexLayout, BlockLayout, Card, SideNav, Button, InputWrapper }
})

export default class Home extends App {

    sideNavPages             = App.getSideNavPages('');
    somenteSolucoesPositivas = false;
    equacao: string          = '';

    solucaoGeral             = { x: [], y: [] };
    solucoes                 = [];

    calcular() {
        try {
            let obj = this.obterValoresDaEquacao();
            this.solucaoGeral = this.resolverEquacao(obj.a, obj.b, obj.c);
            this.encontrarSolucoes();
        } catch (e) {
            new M.Toast({ 'html': e });
        }
    }

    obterValoresDaEquacao() {
        let operator = this.equacao.indexOf('+') !== -1 ? '+' : '-';
        let eq = this.equacao.replace('x', 'x ').replace('=', '= ').replace('y', 'y ').replace(operator, operator + ' ');
        let a  = Number(eq.split('x')[0].replace(' ', '') || 0);
        let b  = Number(eq.split('x')[1].split('=')[0].split(operator)[1].replace(' ', '').split('y')[0] || 0);
        let c  = Number(eq.split(' = ')[1].replace(' ', '') || 0);
        if (Number.isNaN(a) || Number.isNaN(b) || Number.isNaN(c)) throw Error('Sua equação está em um formato inválido. Verifique a página Sobre para mais informações.');
        return {
            'a': a,
            'b': operator === '+' ? b : -b,
            'c': c
        }
    }

    resolverEquacao(a: number, b: number, c: number) {
        let passosEuclides = [];
        let mdc            = 0;

        // Algoritmo de euclides para calcular MDC
        let maior = Math.max(a, b),
            menor = Math.min(a, b);

        while (!mdc) {
            let dividendo = !!passosEuclides[0] ? passosEuclides[passosEuclides.length - 1].divisor : maior;
            let divisor   = !!passosEuclides[0] ? passosEuclides[passosEuclides.length - 1].resto   : menor;
            let quociente = (dividendo / divisor) - ((dividendo / divisor) % 1);
            let resto     = dividendo % divisor;
            let atual     = { quociente, resto, dividendo, divisor };
            if (atual.resto === 0) mdc = atual.divisor;
            if (!!passosEuclides.length[0] && JSON.stringify(passosEuclides[passosEuclides.length - 1]) === JSON.stringify(atual)) {
                mdc = atual.divisor;
                break;
            }
            passosEuclides.push(atual);
        }
        
        /* Verificando se há solução */
        if (c % mdc !== 0) { throw Error('Não há soluções'); }

        /* Encontrando solução gerais */
        let solucaoParticular = this.encontrarSolucaoParticular(a / mdc, b / mdc);
        let x0 = (c / mdc) * solucaoParticular[0];
        let y0 = (c / mdc) * solucaoParticular[1];

        return { x: [x0, b/mdc], y: [y0, a/mdc] };
    }

    encontrarSolucaoParticular(a: number, b: number) {
        if (a === 0) return [0, 1];

        let r = this.encontrarSolucaoParticular(b % a, a);
        let x = r[0], y = r[1];

        return [(y - (b / a) * x), x];
    }

    encontrarSolucoes() {
        if (this.solucaoGeral && this.solucaoGeral.x.length === 0) return;
        this.solucoes = [];
        let k = this.somenteSolucoesPositivas ? -10000 : 0, 
            count = 0;
        while (this.solucoes.length < 10 && count < 100000) {
            if (this.somenteSolucoesPositivas) {
                if (this.solucaoGeral.x[0] + (this.solucaoGeral.x[1] * k) >= 0 &&
                        this.solucaoGeral.y[0] - (this.solucaoGeral.y[1] * k) >= 0)
                    this.solucoes.push(k);
            } else {
                this.solucoes.push(k);
            }
            k++;
            count++;
        }
    }

}