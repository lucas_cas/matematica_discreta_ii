import Vue from 'vue';
import App from './app/App.vue';
import router from './router';
import store from './store';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';

Vue.config.productionTip = true;
Vue.config.devtools = true;

import UserService from './../users.js/UserService';

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');