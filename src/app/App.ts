import { Component, Prop, Vue } from 'vue-property-decorator';
import './App.scss';
import { FlexDirection } from './../../cas_frontend/components/containers/flex_layout/FlexLayout';
import { MarginOptions, PaddingOptions } from './../../cas_frontend/components/containers/utils/AlignOptions';
import { TransitionSpeed } from './../../cas_frontend/components/containers/card/Card';
import RequestBuilder from './../../cas_frontend/components/request/RequestBuilder';
import { SideNavPage } from 'cas_frontend/components/menus/sidenav/SideNav';

@Component
export default class App extends Vue {

  public flexDirections      = FlexDirection;
  public marginOptions       = MarginOptions;
  public paddingOptions      = PaddingOptions;
  public transitionSpeed     = TransitionSpeed;
  public requestBuilder      = new RequestBuilder('localhost:8083/equacoes_diofantinas');

  static sideNavPages: Array<SideNavPage> = [
    {
        'href': '#/',
        'divider': false,
        'icon': 'mode_edit',
        'name': 'Calcular',
        'selected': false
    },
    {
        'href': '#/about',
        'divider': false,
        'icon': 'info',
        'name': 'Sobre',
        'selected': false
    }
  ];

  mounted() {
  }

  static getSideNavPages(currentPageHref: string) {
    let pages = JSON.parse(JSON.stringify(this.sideNavPages));
    let page  = pages.find((p: any) => p.href === '#/' + currentPageHref);
    if (page) {
      page.selected = true;
      return pages;
    } else console.error('The page with href ' + currentPageHref + ' does not exist.');
  }

}