module.exports = {

    // Project deployment base
    // By default we assume your app will be deployed at the root of a domain,
    // e.g. https://www.my-app.com/
    // If your app is deployed at a sub-path, you will need to specify that
    // sub-path here. For example, if your app is deployed at
    // https://www.foobar.com/my-app/
    // then change this to '/my-app/'
    publicPath: '.',
    
    // where to output built files
    outputDir: 'www',
    
    
    
        //Defines in what addres "npm run serve" will start
        devServer: {
            // Paths
            allowedHosts: [
    
                // Various Dev Server settings
                '192.168.120.1:8080', // can be overwritten by process.env.HOST
    
            ]
        },
    }